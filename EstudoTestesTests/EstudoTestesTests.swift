//
//  EstudoTestesTests.swift
//  EstudoTestesTests
//
//  Created by Michel Anderson Lutz Teixeira on 05/02/19.
//  Copyright © 2019 Michel Anderson Lutz Teixeira. All rights reserved.
//

import XCTest
@testable import EstudoTestes

class EstudoTestesTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

class EstudoTestes_Dog_Tests: XCTestCase {

    private let dogName = "Simba"
    private let dogAge = 4
    private let dogBreed = "Golden"

    func testDogModal_withAllParams_shouldSuccess() {
        let dog: Dog = Dog(name: self.dogName,
                           age: self.dogAge,
                           breed: self.dogBreed)
        print("testDogModal_withAllParams_shouldSuccess")
        XCTAssertEqual(self.dogName, dog.name, "Dog name is different.")
        XCTAssertEqual(self.dogAge, dog.age, "Dog age is different.")
        XCTAssertEqual(self.dogBreed, dog.breed, "Dog breed is different.")
    }

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        print("setUp")
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        print("tearDown")
    }

    func testExample() {
        testDogModal_withAllParams_shouldSuccess()
        print("testExample")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        print("testPerformanceExample")
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
