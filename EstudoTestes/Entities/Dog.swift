//
//  Dog.swift
//  EstudoTestes
//
//  Created by Michel Anderson Lutz Teixeira on 05/02/19.
//  Copyright © 2019 Michel Anderson Lutz Teixeira. All rights reserved.
//

import Foundation

struct Dog: Codable {
    var name: String
    var age: Int
    var breed: String

    init(name: String, age: Int, breed: String) {
        self.name = name
        self.age = age
        self.breed = breed
    }
}
